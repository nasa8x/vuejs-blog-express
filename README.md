# vuejs-blog-express

> A Vue.js Blog with Express.js

## Build Setup

``` bash
# run with localhost
grunt

# when coding
grunt watch

# when development with production, before run you must create file config/env/prod.js and edit config for run on server.
grunt --env=prod

```

This blog use theme [Remark](https://goo.gl/2DuL6c). Share by [Vue.js Vietnam](https://www.facebook.com/Vuejs-Vietnam-1744651725846108/?ref=bookmarks)
