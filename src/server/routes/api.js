
// Invoke 'strict' JavaScript mode
'use strict';

var async = require('async'),
    mongoose = require('mongoose'),
    utils = require('mix-utils'),
    config = require('../config/index'),
    Posts = require('../models/post');

// Define the routes module' method
module.exports = function (app) {

    app.all('/api/*', function (req, res, next) {
        res.type('application/json');
        res.header("Access-Control-Allow-Origin", "*");
        res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
        res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
        next();
    });

    // route middleware to make sure a user is logged in
    function isAuthenticated(req, res, next) {

        // if user is authenticated in the session, carry on
        if (req.isAuthenticated())
            return next();

        // if they aren't redirect them to the Login page
        //return res.redirect('/member/login');

        var err = new Error('Unauthorized');
        //err.status = 401;
        return res.status(401).send(err);
    };

    //app.all('/api/*', isAuthenticated);

    //Get list posts
    app.post('/api/post/fetch', function (req, res) {
        let _kwd = req.body.kwd;
        let _page = parseInt(req.body.page) || 1;
        let _limit = 18;
        //let _tid = req.body.uid ? req.body.uid : req.body._tid;
        let _condition = [];
        let _series = [];

        if (!utils.isEmpty(_kwd)) {
            _condition.push({ tl: new RegExp(_kwd, 'ig') });
        }

        Posts.paginate({ stt: { $gte: 1 } }, {
            select: { tl: 1, img: 1, sid: 1, stt: 1, crt: 1 },
            page: _page,
            limit: _limit,
            sort: { crt: -1 }
        }, (err, result) => {
            if (err) {
                console.log(err);
            }
            if (result) {
                //  console.log(result);
                return res.json(result);
            } else {
                return res.json({});
            }
        });
    });


    //Post detail
    app.post('/api/post/info', (req, res) => {
        let _id = req.body.id;

        Posts.findOne({ sid: _id }, (err, doc) => {
            if (doc) {
                //Update views + 1
                Posts.update({ _id: doc._id }, { $set: { v: doc.v + 1 } }, () => {});

                var _kwd = doc.tags.length > 0 ? doc.tags.join(' ') : doc.tl;
                var _tag = doc.tags.length > 0 ? doc.tags[0] : doc.tl;

                async.parallel({
                        suggest: function (callback) {

                            Posts.find({ $text: { $search: _kwd }, _id: { $ne: doc._id }, stt: { $gte: 1 } }, { score: { $meta: "textScore" } })
                                .sort({ score: { $meta: 'textScore' } })
                                .select({ tl: 1, v: 1, img: 1, sid: 1, stt: 1,crt: 1 })
                                .limit(4)
                                .exec(function (err, docs) {
                                    if (err)
                                        console.log(err);

                                    callback(null, docs);

                                });


                        },
                        recommended: function (callback) {

                            Posts.find({ $text: { $search: _tag }, _id: { $ne: doc._id }, stt: { $gte: 1 }, })
                                .sort({ stt: -1, crt: -1 })
                                .select({ tl: 1, v: 1, img: 1, sid: 1, stt: 1, crt: 1 })
                                .limit(3)
                                .exec(function (err, docs) {
                                    if (err)
                                        console.log(err);

                                    callback(null, docs);
                                });


                        }

                    },
                    function (err, results) {
                        if (err)
                            console.log(err);

                        return res.json({
                            info: doc,
                            suggest: results.suggest,
                            recommended: results.recommended
                        });
                    });



            } else {
                return res.json({});
            }
        })
    });


};
