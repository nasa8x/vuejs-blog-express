// Invoke 'strict' JavaScript mode
'use strict';

// Load the module dependencies
var passport = require('passport'),
    TwitterStrategy = require('passport-twitter').Strategy,
    Member = require('../models/member'),
    config = require('../config');

// Create the Twitter strategy configuration method
module.exports = function () {
    // Use the Passport's Twitter strategy
    passport.use(new TwitterStrategy({
        consumerKey: config.twitter.clientID,
        consumerSecret: config.twitter.clientSecret,
        callbackURL: config.twitter.callbackURL,
        passReqToCallback: true
    },
        function (req, token, tokenSecret, profile, done) {
            var data = profile._json;

            // make the code asynchronous
            // User.findOne won't fire until we have all our data back from Twitter
            process.nextTick(function () {

                Member.findOne({ 'tw.id': profile.id },
                    function (err, user) {
                        // if there is an error, stop everything and return that
                        // ie an error connecting to the database
                        if (err)
                            return done(err);

                        // if the user is found then log them in
                        if (user) {
                            user.tw.token = token;
                            user.tw.uid = profile.username;
                            user.tw.nm = profile.displayName;
                            user.tw.avt = data.profile_image_url;
                            user.save(function (err) {
                                if (err)
                                    throw err;
                                return done(null, user); // user found, return that user
                            });

                        } else {
                            // if there is no user, create them
                            var obj = new Member();

                            // set all of the user data that we need
                            obj.tw.id = profile.id;
                            obj.tw.token = token;
                            obj.tw.uid = profile.username;
                            obj.tw.nm = profile.displayName;
                            obj.avt = data.profile_image_url;

                            // save our user into the database
                            obj.save(function (err) {
                                if (err)
                                    throw err;
                                return done(null, obj);
                            });
                        }
                    });
            });
        }
    ));
};
